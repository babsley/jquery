class jQuery {
    constructor(selector) {
        let elements = document.querySelectorAll(selector);
        this.length = elements.length;

        jQuery.forEach.call(elements, (el, i) => {
            this[i] = el;
        });
    }


    static forEach(fn) {
        if (!fn) {
            return this;
        }

        for (let i = 0; i < this.length; i++) {
            if (fn(this[i], i) === false) {
                return this;
            }
        }
    }

    addClass(value) {
        if (!value) {
            return this;
        }

        let classList = null;

        if (value && typeof value !== 'function') {
            classList = value.split(' ');
        }

        jQuery.forEach.call(this, (el, i) => {
            let className = el.className;
            let newClasses = '';

            if (typeof value === 'function') {
                classList = value(i, el);
                classList = classList ? classList.split(' ') : '';
            }

            for (let i = 0; i < classList.length; i++) {
                if (className.indexOf(classList[i]) >= 0) {
                    continue;
                }

                newClasses = `${newClasses} ${classList[i]}`;
            }

            el.className = className + newClasses;
        });


        return this;
    }

    each(fn) {
        jQuery.forEach.call(this, (el, i) => {
            if (fn.call(el, i, el) === false) {
                return false;
            }
        })

        if (!fn && typeof fn !== 'function') {
            return this;
        }

        return this;
    }
}

window.$ = function (selector) {
    return new jQuery(selector);
};
